# Code Quality

Defines a job `code_quality` that runs CodeClimante to determine the quality of the code.

# How to use

```yaml
include:
  - project: fabiopitino-grp/gitlab-ci-instance-templates/code-quality
    file: .gitlab-ci.yml
    ref: 0.1.0
```

Optionally you can customize the `code_quality` job, like changing the `stage` where it runs, whether you should `allow_failure`, etc.

```yaml
include:
  - project: fabiopitino-grp/gitlab-ci-instance-templates/code-quality
    file: .gitlab-ci.yml
    ref: 0.1.0

code_quality:
  stage: post-test
  allow_failure: false
```
